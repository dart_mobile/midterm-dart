import 'dart:collection';
import 'dart:io';
import 'dart:math';

List splitString(String sentence) {
  List<String> a = sentence.split(" ");
  return a;
}

List splitNoSpace(String sentence) {
  List<String> a = sentence.split('');
  return a;
}
bool isNumeric(String sign) {
  if (sign == "+") {
    return false;
  }
  if (sign == "-") {
    return false;
  }
  if (sign == "*") {
    return false;
  }
  if (sign == "/") {
    return false;
  }
  if (sign == "^") {
    return false;
  }
  if (sign == "(") {
    return false;
  }
  if (sign == ")") {
    return false;
  }
  return true;
}

String infixToPostfix(String sentenceSplit) {
  String postfix = "";
  int precedenceLast = 0;
  int precedenceChar = 0;

  Queue<String> Operator = new Queue<String>();

  for (int i = 0; i < sentenceSplit.length; i++) {
    String character = sentenceSplit[i];
    if (isNumeric(character)) {
      postfix += character;
    } else if (character == "(") {
      Operator.add(character);
    } else if (character == ")") {
      while (Operator.last != "(") {
        postfix += Operator.last;
        Operator.removeLast();
        break;
      }
      Operator.remove("(");
    } else if (!isNumeric(character)) {
      switch (character) {
        case "+":
        case "-":
          {
            precedenceChar = 1;
          }
          break;
        case "*":
        case "/":
          {
            precedenceChar = 2;
          }
          break;
        case "^":
          {
            precedenceChar = 3;
          }
          break;
      }

      if (Operator.isNotEmpty) {
        switch (Operator.last) {
          case "+":
          case "-":
            {
              precedenceLast = 1;
            }
            break;
          case "*":
          case "/":
            {
              precedenceLast = 2;
            }
            break;
          case "^":
            {
              precedenceLast = 3;
            }
            break;
        }
      }

      while (Operator.isNotEmpty && precedenceChar <= precedenceLast) {
        if (Operator.last != "(") {
          postfix += Operator.last;
        }
        Operator.removeLast();
        if (Operator.isNotEmpty) {
          switch (Operator.elementAt(Operator.length - 1)) {
            case "+":
            case "-":
              {
                precedenceLast = 1;
              }
              break;
            case "*":
            case "/":
              {
                precedenceLast = 2;
              }
              break;
            case "^":
              {
                precedenceLast = 3;
              }
              break;
          }
        }
      }
      Operator.add(character);
    }
  }
  while (!Operator.isEmpty) {
    if (Operator.last == "(") {
      Operator.removeLast();
    }
    postfix += Operator.last;
    Operator.removeLast();
  }

  return postfix;
}

Evaluation(List sent) {
  List<double> sum = [];

  for (var i = 0; i < sent.length; i++) {
    if (isNumeric(sent[i])) {
      double num = double.parse(sent[i]);
      sum.add(num);
    } else {
      double total = 0;
      // double R = 15;
      // double L = 1;
      double R = sum.last;
      sum.removeLast();
      double L = sum.last;
      sum.removeLast();

      switch (sent[i]) {
        case "+":
          total = L + R;
          break;
        case "-":
          total = L - R;
          break;
        case "*":
          total = L * R;
          break;
        case "/":
          total = L / R;
          break;
        case "^":
          total = 0.0 + pow(L, R);
          break;
      }
      sum.add(total);
    }
  }
  return sum[0];
}

void main(List<String> args) {
  // 2 * 3 / 5 
  // 1 + 5 ^ 4 - 20  
  print("input your sentence");
  String sentence = stdin.readLineSync()!;
  print("\n1.Split list values is \n  " + splitString(sentence).toString());

  String sentenceSplit = splitString(sentence).join();
  print("2.Postfix this list is \n  " + infixToPostfix(sentenceSplit));


  List postfixList = splitNoSpace(infixToPostfix(sentenceSplit));
  print("3.Answer this postfix is \n  " + Evaluation(postfixList).toString());
}
